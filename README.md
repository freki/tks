# Bird Song Classifier #

* School project for automatic bird song classifying. Supports 10 most numerous species in the Central Europe.
* Version 0.9

## List of supported species ##

Name in czech | Latin name
-------------------|---------------
 Pěnkava obecná  | Fringilla coelebs
 Sýkora koňadra  |  Parus major
 Vrabec domácí | Passer domesticus
 Kos černý | Turdus merula
 Strnad obecný | Emberiza citrinella
 Budníček menší  | Phylloscopus collybita
 Špaček obecný  | Sturnus vulgaris
 Pěnice černohlavá	| Sylvia atricapilla
 Sýkora modřinka | Parus caeruleus
 Skřivan polní	| Alauda arvensis

Current [database](https://bitbucket.org/freki/tks/downloads) is available at download page.
