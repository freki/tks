QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Birds
TEMPLATE = app
CONFIG += c++11

RC_FILE = Birds.rc

SOURCES += main.cpp\
        mainwindow.cpp \
    aquila/transform/Dct.cpp \
    aquila/transform/Dft.cpp \
    aquila/transform/FftFactory.cpp \
    aquila/transform/Mfcc.cpp \
    aquila/source/Frame.cpp \
    aquila/source/FramesCollection.cpp \
    aquila/source/SignalSource.cpp \
    aquila/source/WaveFile.cpp \
    aquila/source/WaveFileHandler.cpp \
    aquila/filter/MelFilter.cpp \
    aquila/filter/MelFilterBank.cpp \
    database.cpp \
    classpattern.cpp \
    classbayes.cpp \
    genericprocess.cpp \
    aquila/ml/Dtw.cpp \
    classdtw.cpp \
    teacher.cpp \
    record.cpp \
    aquila/transform/MyFft.cpp

HEADERS  += mainwindow.h \
    aquila/global.h \
    aquila/filter/MelFilter.h \
    aquila/filter/MelFilterBank.h \
    aquila/source/Frame.h \
    aquila/source/FramesCollection.h \
    aquila/source/SignalSource.h \
    aquila/source/WaveFile.h \
    aquila/source/WaveFileHandler.h \
    aquila/transform/Dct.h \
    aquila/transform/Dft.h \
    aquila/transform/Fft.h \
    aquila/transform/FftFactory.h \
    aquila/transform/Mfcc.h \
    genericprocess.h \
    database.h \
    classpattern.h \
    classbayes.h \
    aquila/ml/Dtw.h \
    aquila/ml/DtwPoint.h \
    aquila/functions.h \
    classdtw.h \
    teacher.h \
    record.h \
    aquila/transform/MyFft.h

FORMS    += mainwindow.ui

DISTFILES +=  Birds.rc
