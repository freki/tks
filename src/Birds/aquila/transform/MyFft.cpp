#define _USE_MATH_DEFINES
#include <cmath>
#include "MyFft.h"
#include <algorithm>
#include <complex>

namespace Aquila
{

const ComplexType MyFft::j(0, 1);

SpectrumType MyFft::fft(const SampleType _x[])
{
  SpectrumType spectrum(N);
  double m = std::log2(N);
  unsigned i, j, k;
  double y[N], x[N];

  // initialize
  for (i = 0; i < N; i++) {
    y[i] = 0.0;
    x[i] = _x[i];
  }

  // bit reversal
  j = 0;
  for (i = 0; i < N-1; i++) {
    if (i < j) {
      std::swap(y[i], y[j]);
      std::swap(x[i], x[j]);
    }
    k = N >> 1;
    while (k <= j) {
      j -= k;
      k >>= 1;
    }
    j += k;
  }

  //FFT
  double c1 = -1.0;
  double c2 = 0.0;
  double u1, u2, t1, t2, z;
  unsigned l2 = 1, l1, i1;
  for (int l = 0; l < m; l++) {
    l1 = l2;
    l2 <<= 1;
    u1 = 1.0;
    u2 = 0.0;
    for (j = 0; j < l1; j++) {
      for (i = j; i < N; i += l2) {
        i1 = i + l1;
        t1 = u1 * x[i1] - u2 * y[i1];
        t2 = u1 * y[i1] + u2 * x[i1];
        x[i1] = x[i] - t1;
        y[i1] = y[i] - t2;
        x[i] += t1;
        y[i] += t2;
      }
      z =  u1 * c1 - u2 * c2;
      u2 = u1 * c2 + u2 * c1;
      u1 = z;
    }
    c2 = sqrt((1.0 - c1) / 2.0);
    c1 = sqrt((1.0 + c1) / 2.0);
  }

  // scale and fill spectrum with values
  for (i = 0; i < N; i++) {
    // x[i] /= N;
    // y[i] /= N;
     spectrum[i] = ComplexType(x[i], y[i]);
  }
  return spectrum;
}

void MyFft::ifft(SpectrumType spectrum, double x[])
{
  //not implemeted
}
}
