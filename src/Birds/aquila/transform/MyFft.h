#ifndef MYFFT_H
#define MYFFT_H

#include "Fft.h"

namespace Aquila
{
/**
* A implementation of the Fast Fourier Transform.
*/
class AQUILA_EXPORT MyFft : public Fft
{
public:
  /**
  * Initializes the transform for a given input length.
  *
  * @param length input signal size
  */
  MyFft(std::size_t length):
    Fft(length)
  {
  }

  /**
  * Destroys the transform object.
  */
  ~MyFft()
  {
  }

  /**
  * Applies the transformation to the signal.
  *
  * @param x input signal
  * @return calculated spectrum
  */
  virtual SpectrumType fft(const SampleType x[]);

  /**
  * Applies the inverse transform to the spectrum.
  *
  * @param spectrum input spectrum
  * @param x output signal
  */
  virtual void ifft(SpectrumType spectrum, double x[]);

private:
  /**
  * Complex unit (0.0 + 1.0j).
  */
  static const ComplexType j;
};
}

#endif // MYFFT_H
