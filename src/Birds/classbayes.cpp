#include "classbayes.h"

void ClassBayes::run()
{
  mfccPercent = 90;
  OpenWavFile();
  CountMfcc();
  clId = CountClass();
  emit progress(100);
}

void ClassBayes::InitProp()
{
  vector<Record>* data = Database::getInstance().GetRecords();

  for (int i = 0; i < (int) eci_count; i++)
    prop[i] = 0.0;

  for (unsigned int i = 0; i < data->size(); i++)
    prop[data->at(i).classId]++;

  for (int i = 0; i < (int) eci_count; i++)
    prop[i] /= data->size();
}

double ClassBayes::CountProp(ClassIdentifier id)
{
  double value = 1.0;
  vector<Record>* data = Database::getInstance().GetRecords();
  double propData[FRAMES * FEATURES];
  int count[FRAMES * FEATURES];

  for (int i = 0; i < FRAMES * FEATURES; i++)
  {
    propData[i] = 0.0;
    count[i] = 0;
  }

  for (unsigned int i = 0; i < data->size(); i++)
  {
    for (unsigned int j = 0; j < FRAMES; j++)
      for (unsigned int k = 0; k < FEATURES; k++)
      {
        double num1 = exp(r.values[j][k]);
        double num2 = exp(data->at(i).values[j][k]);
        if (abs((num1 - num2)/max(num1, num2)) < 1e-5)
        {
          count[j*FEATURES+k]++;
          if (data->at(i).classId == id)
            propData[j*FEATURES+k]+=1.5;
        }
      }
  }

  for (unsigned int j = 0; j < FRAMES; j++)
    for (unsigned int k = 0; k < FEATURES; k++)
  {
    if (propData[j*FEATURES+k] < 1e-12)
      propData[j*FEATURES+k] = 1;

    propData[j*FEATURES+k] = (count[j*FEATURES+k] > 0) ? propData[j*FEATURES+k] / count[j*FEATURES+k] : propData[j*FEATURES+k];
    value *= propData[j*FEATURES+k];
  }

  qDebug("Class %d: Prop is %e * %.2f", id, value, prop[id]);
  return value*prop[id];
}

ClassIdentifier ClassBayes::CountClass()
{
  double actual;
  double best = 0;
  ClassIdentifier id = eci_count;
  // count P(h)
  InitProp();

  for (int i = 0; i < (int) eci_count; i++)
  {
    //count probability it belongs to this class
    actual = CountProp((ClassIdentifier) i);
    if (actual > best && actual > 0.0)
    {
      best = actual;
      id = (ClassIdentifier) i;
    }
    emit progress(i + 90);
  }
  qDebug("Best %e (%d)", best, id);
  return id;
}
