#ifndef CLASIFFIER_H
#define CLASIFFIER_H
#include "genericprocess.h"
#include <math.h>

#define EPS 1e-3

/**
 * The Clasiffier class.
 * Represents the classifying process. Uses Naive Bayes to compute class of given wav file.
 * Inherits from GenericProcess and can be launched in separate thread.
 */
class ClassBayes : public GenericProcess
{
Q_OBJECT
private: 
  double prop[eci_count]; /**< Field with probabilities of classes. */

  /**
   * Initialize probabilities of classes.
   */
  void InitProp();

  /**
   * Computes value of probability,
   * that given data belongs to class with ClassIdentifier id.
   *
   * @param id ClassIdentifier of class
   * @return value of probability
   */
  double CountProp(ClassIdentifier id);

  /**
   * Computes the best class for Record.
   *
   * @return ClassIdentifier of computed class.
   */
  ClassIdentifier CountClass();

public:
  /**
   * Clasiffier constructor.
   */
  ClassBayes() : GenericProcess() { }

protected:
  /**
   * Starts the classifying process.
   * Loads new wav file, extracts features for it
   * and count class.
   */
  void run();
};

#endif // CLASIFFIER_H
