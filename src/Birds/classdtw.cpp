#include "classdtw.h"


void ClassDTW::run()
{
  mfccPercent = 90;
  OpenWavFile();
  CountMfcc();
  clId = CountClass();
  emit progress(100);
}

ClassIdentifier ClassDTW::CountClass()
{
  double actual;
  double best = 1e15;
  ClassIdentifier id = eci_count;
  vector<Record>* data = Database::getInstance().GetRecords();
  Dtw dtw(euclideanDistance, Dtw::Diagonals);
  int step = data->size() / 10;

 for (unsigned int i = 0; i < data->size(); i++)
  {
    actual = dtw.getDistance(r.values, data->at(i).values);
    if (actual < best)
    {
      best = actual;
      id = data->at(i).classId;
    }
    emit progress(i/step + 90);
  }
  qDebug("Best: %e (%d)", best, id);
  return id;
}
