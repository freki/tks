#ifndef CLASSDTW_H
#define CLASSDTW_H
#include "genericprocess.h"
#include "aquila/ml/Dtw.h"

class ClassDTW : public GenericProcess
{
Q_OBJECT

public:
  ClassDTW() : GenericProcess() { }

protected:
  void run();

private:
  /**
   * Computes the closest class for Record.
   *
   * @return ClassIdentifier of computed class.
   */
  ClassIdentifier CountClass();
};

#endif // CLASSDTW_H
