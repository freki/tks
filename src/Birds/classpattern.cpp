#include "classpattern.h"

void ClassPattern::run()
{
  mfccPercent = 90;
  OpenWavFile();
  CountMfcc();
  clId = CountClass();
  emit progress(100);
}

double ClassPattern::GetPatternDistance(ClassIdentifier id)
{
  vector<Record>* data = Database::getInstance().GetRecords();
  double distance = 0.0;
  long count = 0;

  //qDebug("Starting class %d", id);
  for(unsigned int i = 0; i < data->size(); i++)
  {
    if (data->at(i).classId == id) // only actual class
    {
      for (int j = 0; j < FRAMES; j++)
        for (int jj = j; jj < FRAMES; jj++)
        {
          count++;
          for (int k = 0; k < FEATURES; k++)
          {
            double num1 = exp(r.values[j][k]);
            double num2 = exp(data->at(i).values[jj][k]);
            distance += abs((num1 - num2) / max(num1, num2));
          }
        }
    }
  }
 // qDebug("Ended class %d", id);
  distance = (count > 0) ? distance / (double) count : -1;
  return distance;
}

ClassIdentifier ClassPattern::CountClass()
{
  ClassIdentifier id = eci_count;
  double actual, best = 1e10;
  for(int i = 0; i < (int) eci_count; i++)
  {
    actual = GetPatternDistance((ClassIdentifier) i);
    if (actual < best && actual > 0.0)
    {
      best = actual;
      id = (ClassIdentifier) i;
    }
    qDebug("Class %d: pattern distance %e", i, actual);
    emit progress(i + 90);
  }
  qDebug("Best: %e (%d)", best, id);
  return id;
}

