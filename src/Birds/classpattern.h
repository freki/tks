#ifndef CLASSPATTERN_H
#define CLASSPATTERN_H
#include "genericprocess.h"


class ClassPattern : public GenericProcess
{
private:
  ClassIdentifier CountClass();
  double GetPatternDistance(ClassIdentifier id);

public:
  /**
   * ClassPattern constructor.
   */
  ClassPattern() : GenericProcess() { }

  /**
   * Starts the classifying process.
   * Loads new wav file, extracts features for it
   * and count class.
   */
  void run();
};

#endif // CLASSPATTERN_H
