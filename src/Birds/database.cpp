#include "database.h"
#include <QDataStream>

Database::Database()
{
  QFile file(DATAFILE);
  if(!file.open(QIODevice::ReadOnly))
  {
    count = 0;
    return;
  }

  QDataStream in;
  in.setDevice(&file);
  in.setFloatingPointPrecision(QDataStream::DoublePrecision);
  in.setVersion(QDataStream::Qt_5_4);
  count = ReadHead(&in);
  ReadRecords(&in);

  file.close();
}

int Database::ReadHead(QDataStream* in)
{
  int fr, fe, c;
  (*in) >> c >> fr >>fe;
  if (fr != FRAMES || fe != FEATURES) // not actual data file
  {
    qWarning("Loaded data file is old. No training data will be used.\n");
    return 0;
  }
  return c;
}

void Database::ReadRecords(QDataStream* in)
{
  Record r;

  for (unsigned int i = 0; i < count; i++)
  {
    (*in) >> r;
    data.push_back(r);
  }
}

void Database::WriteFile()
{
  if (data.size() == count)
  {
    return;
  }
  QFile file(DATAFILE);
  if (!file.open(QIODevice::WriteOnly))
  {
    qCritical("Cannot save data to memmory file.\n");
    return;
  }
  QDataStream out;
  out.setFloatingPointPrecision(QDataStream::DoublePrecision);
  out.setDevice(&file);
  out.setVersion(QDataStream::Qt_5_4);
  out << data.size() << FRAMES << FEATURES;

  for (unsigned int i = 0; i < data.size(); i++)
  {
    out << data.at(i);
  }
  file.flush();
  file.close();
}
