#ifndef DATABASE_H
#define DATABASE_H
#include <stdio.h>
#include <vector>
#include <stdlib.h>
#include <QDebug>
#include <QFile>
#include <QDataStream>
#include "record.h"

#define DATAFILE "memmory.dat"

using namespace std;

/**
 * The Database class.
 * Represents the program memmory. Contains all learn record.
 *
 * Database is a singleton, there can be just one instance
 * with common data for all users (ie. Teacher and Classifier).
 *
 * Loads records from file on statup and save if something changed on exit.
 */
class Database
{
private:
  vector<Record> data; /**< Loaded 'memmory'. Training set. */
  bool newFile; /**< Flag if file existed. */
  unsigned int count; /**< Count of elements loaded from file. */

  /**
   * Database constructor.
   */
  Database();

  /**
  *  Database destructor.
  */
  ~Database() { WriteFile(); }

  /**
   * Reads the head of data file, which containts
   * count of element whit frames and features
   * count.
   *
   * @return count of elements in data file
   */
  int ReadHead(QDataStream* in);

  /**
   * Reads the records from data file.
   */
  void ReadRecords(QDataStream* in);

  /**
   * Writes records to the file.
   * Checks if new records were added, and if so, write cached data into file.
   */
  void WriteFile();
public:
  /**
   * Gets the only instance of Database.
   * If needed, instance is created.
   *
   * @return only instance of Database
   */
  static Database& getInstance()
  {
    static Database instance;
    return instance;
  } 

  /**
   * Deleted copy constructor.
   */
  Database(Database const&) = delete;

  /**
   * Deleted copy constructor.
   */
  void operator=(Database const&) = delete;

  /**
   * Gets the memmory of program.
   * @return vektor with records
   */
  inline vector<Record>* GetRecords() { return &data; }

  /**
   * Learns the new Record. Adds Record to database.
   * @param r new Record to be learned.
   */
  inline void AddRecord(const Record* r) { data.push_back(*r); }
};

#endif // DATABASE_H
