#include "genericprocess.h"

void GenericProcess::CountMfcc() {
  const double treshold = power(file);
  FramesCollection frames(file, SIZE, SIZE/16);
  Mfcc mfcc(SIZE);
  r.values.clear();

  for (Frame& frame : frames) {
    if (r.values.empty() && energy(frame) < treshold)
      continue;
    auto mfccValues = mfcc.calculate(frame, FEATURES);
    r.values.push_back(mfccValues);

    emit progress((r.values.size()*mfccPercent - 1)/FRAMES);

    if (r.values.size() == FRAMES)
      break;
  }

  //padding too small file with 0.0
  if (r.values.size() < FRAMES)
    for (unsigned int i = r.values.size()-1; i < FRAMES; i++)
    {
      r.values.push_back(vector<double>(12));
    }

  double max = 0.0, min = file.getSampleFrequency();
  for (int i = 0; i < FRAMES; i++)
   for (int j = 0; j < FEATURES; j++)
   {
    if (r.values[i][j] > max)
      max = r.values[i][j];
    if (r.values[i][j] < min)
      min = r.values[i][j];
  }

 for (int i = 0; i < FRAMES; i++)
   for (int j = 0; j < FEATURES; j++)
    r.values[i][j] /= std::max(abs(min), max);

 emit progress(mfccPercent);
}
