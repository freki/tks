#ifndef PROCESS
#define PROCESS

#include "aquila/global.h"
#include "aquila/transform/Mfcc.h"
#include "aquila/source/FramesCollection.h"
#include "aquila/source/Frame.h"
#include "aquila/source/WaveFile.h"

#define SIZE 2048

#include <QThread>
#include <QString>
#include "database.h"

using namespace Aquila;

/**
 * The GenericProcess class.
 * Abstract class of learning or teaching process.
 * Launches in separate thread.
 */
class GenericProcess : public QThread {
Q_OBJECT
signals:
  void progress(int p); /**< Signal of work completation progress. */

protected:
  int mfccPercent; /**< Count of percent after MFFC computing. Used by classifier. */
  QString name; /**< Name of wav file to open. */
  WaveFile file; /**< Opened wav file. */
  Record r; /**< Actual record, which is computed. */
  ClassIdentifier clId; /**< ClassIdentifier of actual Record. Computed (when classifying) or given by teacher (when learning). */

  /**
   * Opens the wav file.
   */
  inline void OpenWavFile() { file.load(name.toStdString(), LEFT); }

  /**
   * Extracts features for loaded sound.
   * Use Aquila::Mfcc to calculate features.
   */
  void CountMfcc();
  /**
   * Runs the GenericProcess in new thread.
   */
  virtual void run() = 0;

public:

  /**
   * GenericProcess constructor.
   */
  GenericProcess() : QThread() { }

  /**
   * GenericProcess destructor.
   */
  virtual ~GenericProcess() { }

  /**
   * Gets the computed ClassIdentifier.
   * @return computed ClassIdentifier
   */
  inline ClassIdentifier GetClass() { return clId; }

  /**
   * Sets the ClassIdentifier for current Record.
   * @param i current ClassIdentifier
   */
  inline void SetClass(ClassIdentifier i) { clId = i; }

  /**
   * Sets the name of wav file.
   * @param name name of wav file
   */
  inline void FileName(QString name) { this->name = name; }
};

#endif // PROCESS

