#ifdef _WIN32
#define PATH "../../../sounds"
#else
#define PATH "..\..\..\sounds"
#endif

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent), ui(new Ui::MainWindow)
{
  path = PATH;
  teacher = new Teacher();
  classifierNBK = new ClassBayes();
  classifierDWT = new ClassDTW();
  clType = ecl_nbk;
  ui->setupUi(this);
  setWindowIcon(QIcon(":/icon.ico"));
  initActions();
  initMenu();
  initConnections();  
  EnableControls(true);
  ui->status->showMessage("Připraveno.");
}

MainWindow::~MainWindow()
{
  delete teacher;
  delete classifierDWT;
  delete classifierNBK;
  delete ui;
}

void MainWindow::closeEvent( QCloseEvent *event )
{
  if(isSafeToClose())
    event->accept();
  else
    event->ignore();
}

bool MainWindow::isSafeToClose()
{
  if (teacher->isRunning() || classifierDWT->isRunning() || classifierNBK->isRunning())
  {
    switch(QMessageBox::warning(this, tr("Klasifikátor"),
      tr("Probíhá výpočet. Chcete opravdu přerušit výpočet?"),
         QMessageBox::Yes | QMessageBox::Cancel ) )
    {
    case QMessageBox::Yes:
      if (teacher->isRunning())
        teacher->exit(1);
      if (classifierDWT->isRunning())
        classifierDWT->exit(1);
      if (classifierNBK->isRunning())
        classifierNBK->exit(1);
      return true;
    default:
      return false;
    }
  }
  return true;
}

void MainWindow::openFile()
{
  QFileDialog dialog(this, QString(), QString(), tr("Wav files (*.wav)"));
  dialog.setFileMode(QFileDialog::ExistingFiles);  
  files = dialog.getOpenFileNames(this, tr("Vyberte soubor(y)"), path, tr("Zvuk ve formátu wav (*.wav)"));

  fileIndex = -1;
  if (!files.isEmpty())
  {
    ui->lblFile->setText(files.at(0));
    path = files.at(0);
    ui->btnStart->setEnabled(true);
  } else
  {
    ui->lblFile->setText("<Otevřete soubor s nahrávkou>");
    ui->btnStart->setEnabled(false);
    ui->prbValue->setValue(0);
  }
  ui->status->clearMessage();
}

void MainWindow::aboutProg()
{
  QMessageBox::about(this, tr("O aplikaci"),
                     tr("Aplikace na základě nahrávky zpěvu rozpoznává 10 vybraných druhů pěvců "
                        "trvale hnízdící na území ČR.\n\nAutor:\tKateřina Štollová\n(c) 2015\n"
                        "Západočeská univerzita v Plzni\nFakulta aplikovaných věd: Katedra informatiky a výpočetní techniky"));
}

void MainWindow::changeMode()
{
  ui->cmbClasses->setEnabled(ui->chbLearn->checkState() == Qt::Checked);
}

void MainWindow::startProcess()
{
  EnableControls(false);
  fileIndex = 0;
  LaunchNext();
}

void MainWindow::LaunchNext()
{
  ui->prbValue->setValue(0);
  ui->status->showMessage("Inicializuji..");
  if (ui->chbLearn->checkState() == Qt::Checked)
  {
    ui->txtOutput->appendPlainText("Učím se " + files.at(fileIndex) + "  (" +QString::number(fileIndex+1) + "/" + QString::number(files.size()) + ")");
    teacher->SetClass((ClassIdentifier)(ui->cmbClasses->currentIndex()));
    teacher->FileName(files.at(fileIndex));
    teacher->start();
  } else
  {
    ui->txtOutput->appendPlainText("Klasifikuji " + files.at(fileIndex) + "  (" +QString::number(fileIndex+1) + "/" + QString::number(files.size()) + ")");
    switch(clType)
    {
    case ecl_1nn: classifierDWT->FileName(files.at(fileIndex));
      classifierDWT->start(); break;
    default: classifierNBK->FileName(files.at(fileIndex));
      classifierNBK->start(); break;
    }
  }
}

void MainWindow::checkProgress(int value)
{
  ui->status->clearMessage();
  if (value == ui->prbValue->maximum())
  {
    if (ui->chbLearn->checkState() == Qt::Unchecked)
    {
      int id;
      switch(clType)
      {
      case ecl_1nn: id = classifierDWT->GetClass(); break;
      default: id = classifierNBK->GetClass(); break;
      }
      if (id < (int) eci_count)
        ui->txtOutput->appendPlainText("Zvuk rozpoznán jako: " + ui->cmbClasses->itemText(id) + "\n");
      else
        ui->txtOutput->appendPlainText("Nepodařilo se určit zvuk.\n");
    }
    if (fileIndex < files.size()-1)
    {
      fileIndex++;
      LaunchNext();
    } else
    {
      EnableControls(true);
      ui->status->showMessage("Hotovo.");
    }
  }
}

void MainWindow::EnableControls(bool enable)
{
  actOpen->setEnabled(enable);
  actClass1->setEnabled(enable);
  actClass2->setEnabled(enable);
  actstart->setEnabled(enable && !files.empty());
  ui->btnStart->setEnabled(enable && !files.empty());
  ui->btnOpenFile->setEnabled(enable);
  ui->cmbClasses->setEnabled(enable && ui->chbLearn->checkState() == Qt::Checked);
  ui->chbLearn->setEnabled(enable);
  ui->rdb1Nn->setEnabled(enable);
  ui->rdbNbk->setEnabled(enable);
}

void MainWindow::changeClassifier()
{
  if ((ui->rdbNbk->isChecked() && clType == ecl_nbk) || (ui->rdb1Nn->isChecked() && clType == ecl_1nn))
    return;
  if (ui->rdbNbk->isChecked())
  {
    clType = ecl_nbk;
    qDebug("Now using NBK");
  } else
  {
    clType = ecl_1nn;
    qDebug("Now using 1-NN");
  }
}

void MainWindow::initActions()
{
  actOpen = new QAction(tr("&Otevřít soubor"), this);
  actOpen->setShortcut( tr("Ctrl+O") );
  actOpen->setStatusTip( tr("Otevřít soubor(y) s nahrávkou zpěvu."));
  connect(actOpen, SIGNAL(triggered()), this, SLOT(openFile()));

  actMode = new QAction(tr("Učení"), this);
  actMode->setShortcut( tr("Ctrl+U"));
  connect(actMode, SIGNAL(triggered()), this, SLOT(changeMode()));

  actstart = new QAction(tr("Spustit"), this);
  actstart->setShortcut( tr("Ctrl+E"));
  actstart->setStatusTip( tr("Spustit učení nebo klasifikaci."));
  connect(actstart, SIGNAL(triggered()), this, SLOT(startProcess()));

  actAbout = new QAction(tr("O Programu"), this);
  actAbout->setStatusTip( tr("Zobrazit informace o programu."));
  connect(actAbout, SIGNAL(triggered()), this, SLOT(aboutProg()));

  actExit = new QAction(tr("Konec"), this);
  actExit->setShortcut( tr("Ctrl+X"));
  actExit->setStatusTip( tr("Ukončit program."));
  connect(actExit, SIGNAL(triggered()), this, SLOT(close()));

  actClear = new QAction(tr("Smazat výstup"), this);
  actClear->setStatusTip( tr("Smazat výstup."));
  connect(actClear, SIGNAL(triggered()), this, SLOT(clearOutput()));

  QActionGroup* group = new QActionGroup(this);

  actClass1 = new QAction(tr("Klasifikace NBK"), this);
  actClass1->setShortcut( tr("Ctrl+1"));
  actClass1->setStatusTip( tr("Klasifikace pomocí bayesovského klasifikátoru."));
  actClass1->setCheckable(true);
  connect(actClass1, SIGNAL(triggered()), ui->rdbNbk, SLOT(click()));

  actClass2 = new QAction(tr("Klasifikace 1-NN"), this);
  actClass2->setShortcut( tr("Ctrl+2"));
  actClass2->setStatusTip( tr("Klasifikace podle nejbližšího souseda."));
  actClass2->setCheckable(true);
  connect(actClass2, SIGNAL(triggered()), ui->rdb1Nn, SLOT(click()));

  actClass1->setActionGroup(group);
  actClass2->setActionGroup(group);

  actClass1->setChecked(true);
}

void MainWindow::initConnections()
{
  QObject::connect(teacher, SIGNAL(progress(int)), ui->prbValue, SLOT(setValue(int)), Qt::QueuedConnection);
  QObject::connect(classifierNBK, SIGNAL(progress(int)), ui->prbValue, SLOT(setValue(int)), Qt::QueuedConnection);
  QObject::connect(classifierDWT, SIGNAL(progress(int)), ui->prbValue, SLOT(setValue(int)), Qt::QueuedConnection);
  QObject::connect(ui->btnOpenFile, SIGNAL(clicked()), this, SLOT(openFile()));
  QObject::connect(ui->btnStart, SIGNAL(clicked()), this, SLOT(startProcess()));
  QObject::connect(ui->chbLearn, SIGNAL(clicked()), this, SLOT(changeMode()));
  QObject::connect(ui->prbValue, SIGNAL(valueChanged(int)), this, SLOT(checkProgress(int)));
  QObject::connect(ui->rdb1Nn, SIGNAL(clicked()), this, SLOT(changeClassifier()));
  QObject::connect(ui->rdbNbk, SIGNAL(clicked()), this, SLOT(changeClassifier()));
  QObject::connect(ui->btnClear, SIGNAL(clicked()), this, SLOT(clearOutput()));
}

void MainWindow::initMenu()
{
  QMenu *menu;

  menu = menuBar()->addMenu(tr("&Soubor"));
  menu->addAction(actOpen);
  menu->addSeparator();
  menu->addAction(actExit);

  menu = menuBar()->addMenu(tr("&Program"));
  menu->addAction(actstart);
  menu->addSeparator();
  menu->addAction(actClass1);
  menu->addAction(actClass2);
  menu->addSeparator();
  menu->addAction(actClear);

  menu = menuBar()->addMenu(tr("&Nápověda"));
  menu->addAction(actAbout);
}

void MainWindow::clearOutput()
{
    ui->txtOutput->clear();
}
