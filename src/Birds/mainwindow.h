#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMenuBar>
#include <QCloseEvent>
#include <QMessageBox>
#include <QFileDialog>
#include <QThread>

#pragma once
#include "genericprocess.h"
#include "teacher.h"
#include "classbayes.h"
#include "classdtw.h"
#include "classpattern.h"


enum Classifier
{
  ecl_nbk = 0,
  ecl_1nn,
  ecl_pattern
};

namespace Ui {
class MainWindow;
}

/**
 * The MainWindow class.
 * Container for graphical user interface.
 */
class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  /**
  * MainWindow constructor.
  * @param parent parent object
  */
 MainWindow(QWidget *parent);

 /**
 * MainWindow destructor.
 */
 ~MainWindow();

protected:
 /**
   * Performs actions before closing.
   * Checks if some worker thread is still running and informs user.
   * @param event close event
   */
  void closeEvent(QCloseEvent *event);

private slots:
  /**
   * Opens the file(s) to be processed.
   */
  void openFile();

  /**
   * Opens window with information
   * about application.
   */
  void aboutProg();

  /**
   * Changes mode of application.
   * Modes are learning or clasiffying.
   */
  void changeMode();

  /**
   * Starts the process according actual mode.
   */
  void startProcess();

  /**
   * Checks if the worker thread has
   * finished and shows results to user.
   * @param value progress of worker thread job
   */
  void checkProgress(int value);

  /**
   * Changes the Classiffier algorithm.
   */
  void changeClassifier();

  /**
   * Clears the output text field.
   */
  void clearOutput();

private:
  Classifier clType; /**< Type of used classifier. */
  QString path; /**< Last used path. */
  Ui::MainWindow *ui; /**< User interface. */
  GenericProcess* teacher; /**< Teaching process. */
  GenericProcess* classifierNBK; /**< Classifying process. NBK Classifier. */
  GenericProcess* classifierDWT; /**< Classifying process. 1-NN Classifier. */
  QStringList files; /**< Opened files. Are processed one by one. */
  int fileIndex; /**< Index of file to be processed. */

  /**
   * Prepares actions for MainWindow.
   */
  void initActions();

  /**
   * Prepares Menu for MainWindow.
   */
  void initMenu();

  /**
   * Connects actions and slots.
   */
  void initConnections();

  /**
   * Launches right process for next file in files.
   */
  void LaunchNext();

  /**
   * Enables or disables controls.
   * @param enable if true controls are enabled,
   * if false constrols are disabled
   */
  void EnableControls(bool enable);

  /**
   * Checks if any worker thread is
   * running and ask user if exit anyway.
   * @return true if no thread is running or user want to terminate it, false otherwise.
   */
  bool isSafeToClose();

  QAction *actOpen; /**< Action open file(s). */
  QAction *actMode; /**< Action change mode. */
  QAction *actstart; /**< Action start process. */
  QAction *actAbout; /**< Action open about. */
  QAction *actExit; /**< Action exit program. */
  QAction *actClear; /**< Action clear. */
  QAction *actClass1; /**< Action change classifier. */
  QAction *actClass2; /**< Action change classifier. */
};

#endif // MAINWINDOW_H
