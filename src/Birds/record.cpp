#include "record.h"
#include <QDataStream>

QDataStream& operator<<(QDataStream &out, const Record &obj)
{
  for(int i = 0; i < FRAMES; i++)
  {
    for (int j = 0; j < FEATURES; j++)
    {
      out << obj.values.at(i).at(j);
    }
  }
  out << (quint32) obj.classId;
  return out;
}

QDataStream& operator>>(QDataStream &in, Record &obj)
{
  double val;
  quint32 tmp;
  obj.values.resize(FRAMES);
  for(int i = 0; i < FRAMES; i++)
    obj.values[i].resize(FEATURES);

  for(int i = 0; i < FRAMES; i++)
  {
    for (int j = 0; j < FEATURES; j++)
    {
      in >> val;
      obj.values[i][j] = val;

    }
  }

  in >> (quint32&)tmp;
  obj.classId = (ClassIdentifier) tmp;
  return in;
}
