#ifndef RECORD_H
#define RECORD_H

#include <QObject>
#include <vector>

#define FRAMES 200
#define FEATURES 12

/**
 * The ClassIdentifier enum.
 * Represents the class ID for known birds.
 */
enum ClassIdentifier
{
  eci_penkava = 0,
  eci_sykoraK = 1,
  eci_vrabec = 2,
  eci_kos = 3,
  eci_strnad = 4,
  eci_budnicek = 5,
  eci_spacek = 6,
  eci_penice = 7,
  eci_sykoraM = 8,
  eci_skrivan = 9,
  eci_count
};


using namespace std;

/**
 * The Record class contains features and class
 * of a sound record.
 */
class Record
{
public:
  vector<vector<double>> values; /**< Computed features for record. */
  ClassIdentifier classId; /**< ClassIdentifier of class, to which record belongs. */

  /**
   * Record constructor.
   */
  Record() {classId = eci_count; }

  /**
   * Operator << used for serialization.
   *
   * @param ds output data stream
   * @param obj Record to be serizalized
   * @return data stream
   */
  friend QDataStream &operator<<(QDataStream &ds, const Record &obj);

  /**
   * Operator >> used for deserialization.
   *
   * @param ds input data stream
   * @param obj Record to be deserizalized
   * @return data stream
   */
  friend QDataStream &operator>>(QDataStream &ds, Record &obj) ;
};



#endif // RECORD_H
