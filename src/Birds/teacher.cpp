#include "teacher.h"

void Teacher::run()
{
  mfccPercent = 99;
  OpenWavFile();
  CountMfcc();
  r.classId = clId;
  Database::getInstance().AddRecord(&r);
  emit progress(100);
}
