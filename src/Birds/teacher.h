#ifndef TEACHER_H
#define TEACHER_H
#include "genericprocess.h"

/**
 * The Teacher class.
 * Represents the teaching process of program. Extracts features from loaded file
 * and save new Record to Database.
 * Inherits from GenericProcess and can be launched in separate thread.
 */
class Teacher : public GenericProcess
{
Q_OBJECT
public:
  /**
   * Teacher constructor.
   */
  Teacher() : GenericProcess() { }

protected:
  /**
   * Starts the learning process.
   * Loads new wav file, extracts features for it, sets
   * the class given by user and save new Record to Database.
   */
  void run();
};

#endif // TEACHER_H
